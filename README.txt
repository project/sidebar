Readme
------

This module adds support for the sidebar found in Mozilla (yes kids, that 
includes Netscape 6) and Opera browsers. A panel is displayed which lists 
recent nodes and provides links to the full text of the nodes on your drupal 
site.

Send comments to gnudist@gnudist.org.